<?php

class Teachers extends CI_Controller{

  function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Teacher');
    }

    public function index(){

      $data['main_view'] = "teacher/index";
      $data['teachers'] = $this->Teacher->get_all_teachers();

      $this->load->view('layouts/main', $data);
    }

    public function add(){

        if($_POST){
          $data = array();
          $data['name'] = $this->input->post('name', true);
          $data['teacher_id'] = $this->input->post('teacher_id', true);
          $data['phone'] = $this->input->post('phone', true);
          $data['designation'] = $this->input->post('designation', true);
          $data['email'] = $this->input->post('email', true);
          $data['address'] = $this->input->post('address', true);
          $data['age'] = $this->input->post('age', true);
          $data['gender'] = $this->input->post('gender', true);
          $data['school'] = $this->input->post('school', true);
          $data['is_fulltime'] = $this->input->post('is_fulltime', true);
          $data['division'] = $this->input->post('division', true);
          $data['district'] = $this->input->post('district', true);
          $data['thana'] = $this->input->post('thana', true);
          $data['union'] = $this->input->post('union', true);


          if ($this->Teacher->add_teachers($data)) {
            $sdata['message'] = "You Successfully Added Teacher Info !";
            redirect("Teachers/index");
          }
          else {
            $sdata['exception'] = "Sorry Failed to Add Teacher Info !";
            redirect("Teachers/index");
          }

        }else{
          $data = array();
          $data['main_view'] = "teacher/add";
          $data['division'] = $this->Teacher->get_all_division();
          $data['district'] = $this->Teacher->get_all_district_by_divisionid(0);
          $data['thana'] = $this->Teacher->get_all_thana_by_districtid(0);
          $data['union'] = $this->Teacher->get_all_union_by_thanaid(0);

          $this->load->view('layouts/main', $data);
        }
    }

    function edit($id=null){
      if($_POST){
        $data = array();
        $data['id'] = $this->input->post('id', true);
        $data['name'] = $this->input->post('name', true);
        $data['teacher_id'] = $this->input->post('teacher_id', true);
        $data['phone'] = $this->input->post('phone', true);
        $data['designation'] = $this->input->post('designation', true);
        $data['email'] = $this->input->post('email', true);
        $data['address'] = $this->input->post('address', true);
        $data['age'] = $this->input->post('age', true);
        $data['gender'] = $this->input->post('gender', true);
        $data['school'] = $this->input->post('school', true);
        $data['is_fulltime'] = $this->input->post('is_fulltime', true);
        $data['division'] = $this->input->post('division', true);
        $data['district'] = $this->input->post('district', true);
        $data['thana'] = $this->input->post('thana', true);
        $data['union'] = $this->input->post('union', true);

        if ($this->Teacher->edit_teachers($data)) {
          $sdata['message'] = "You Successfully Updated Teacher Info !";
          redirect("Teachers/index");
        }
        else {
          $sdata['exception'] = "Sorry Failed to Update Office Info !";
          redirect("Teachers/index");
        }

      }else{
        $data = array();
        $data['main_view'] = "teacher/edit";
        $data['teachers_info'] = $this->Teacher->get_teachers_by_id($id);

        $this->load->view('layouts/main', $data);
      }
    }


    function delete($id)
        {
          echo $this->Teacher->delete_teachers_id($id);
          if ($this->Teacher->delete_teachers_id($id)) {
            $sdata['message'] = "You Successfully Deleted Teacher Info !";
            redirect("Teachers/index");
          }
    }

    function get_all_district_by_divisionid()
  {
    $division = $this->input->get('division', true);
    $data = array();
    $data['district_info'] =$this->Teacher->get_all_district_by_divisionid($division);
    $this->load->view('teacher/district_list', $data);
  }

    function get_all_thana_by_districtid()
  {
    $district = $this->input->get('district', true);
    $data = array();
    $data['district_info'] =$this->Teacher->get_all_thana_by_districtid($district);
    $this->load->view('teacher/district_list', $data);
  }

  function get_all_union_by_thanaid()
  {
    $thana = $this->input->get('thana', true);
    $data = array();
    $data['district_info'] =$this->Teacher->get_all_union_by_thanaid($thana);
    $this->load->view('teacher/district_list', $data);
  }


    }


 ?>
