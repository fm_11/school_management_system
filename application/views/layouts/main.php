<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Main Page</title>
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
  </head>
  <body>
    <div class="contain-fluid">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
<a class="navbar-brand" href="<?php echo base_url(); ?>Home/index/">Home Page</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
  <div class="navbar-nav">
    <a class="nav-item nav-link active" href="<?php echo base_url(); ?>Class_periods/index/">Class Period <span class="sr-only">(current)</span></a>
    <a class="nav-item nav-link" href="<?php echo base_url(); ?>Teachers/index/">Teacher</a>
    <a class="nav-item nav-link" href="#">Pricing</a>
  </div>
</div>
</nav>
    </div>
    <div class="container">

      <div class="col-xs-12">
        <?php $this->load->view($main_view); ?>
      </div>
    </div>

  </body>
</html>
