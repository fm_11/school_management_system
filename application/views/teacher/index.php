<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Teacher Info</title>
  </head>
  <br>
  <h3 align="center">Teacher Information</h3>
  <body>
    <div class="contain-fluid">
      <a class="btn btn-lg btn-primary" style="float: right;" href="<?php echo base_url(); ?>Teachers/add/" role="button">Add New Teacher</a>
      <br>
      <br>

        <div class="col-12 mb-4">
                                    <h5 class="card-title">Teachers Information</h5>

                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">Teacher ID</th>
                                                <th scope="col">Phone Number</th>
                                                <th scope="col">Designation</th>
                                                <th scope="col">Email</th>
                                                <th scope="col">Address</th>
                                                <th scope="col">School Name</th>
                                                <th scope="col">Age</th>
                                                <th scope="col">Gender</th>
                                                <th scope="col">Is Fulltime?</th>
                                                <!-- <th scope="col">Division</th>
                                                <th scope="col">District</th>
                                                <th scope="col">Thana</th>
                                                <th scope="col">Union</th> -->
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                          $i = 0;
                                          foreach ($teachers as $row):
                                              $i++;
                                              ?>
                                            <tr>
                                                <th scope="row"><?php echo $i; ?></th>
                                                <td><?php echo $row['name']; ?></td>
                                                <td><?php echo $row['teacher_id']; ?></td>
                                                <td><?php echo $row['phone']; ?></td>
                                                <td><?php echo $row['designation']; ?></td>
                                                <td><?php echo $row['email']; ?></td>
                                                <td><?php echo $row['address']; ?></td>
                                                <td><?php echo $row['school']; ?></td>
                                                <td><?php echo $row['age']; ?></td>
                                                <td><?php echo $row['gender']; ?></td>
                                                <td><?php if ($row['is_fulltime']=="0"): ?>
                                                  <?php echo "No"; ?>
                                                <?php else: echo "Yes"; ?>

                                                <?php endif; ?></td>
                                                <!-- <td><?php echo $row['division']; ?></td>
                                                <td><?php echo $row['district']; ?></td>
                                                <td><?php echo $row['thana']; ?></td>
                                                <td><?php echo $row['union']; ?></td> -->


                                                <td>
                                                      <a href="<?php echo base_url(); ?>Teachers/edit/<?php echo $row['id']; ?>"
                                                          title="Edit">
                                                          <button type="button" class="btn btn-primary btn-xs mb-1">
                                                            Edit
                                                          </button>
                                                        </a>

                                                        <a href="<?php echo base_url(); ?>Teachers/delete/<?php echo $row['id']; ?>"
                                                           title="Delete">
                                                           <button type="button" class="btn btn-primary btn-xs mb-1">
                                                             Delete
                                                           </button>

                                                         </a>

                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                        </div>
    </div>


  </body>
</html>
