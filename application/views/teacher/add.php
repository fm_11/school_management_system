<script type="text/javascript">
function get_all_district_by_divisionid(division){
      // alert(division);
      if (window.XMLHttpRequest)
      {
          xmlhttp = new XMLHttpRequest();
      }
      else
      {
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function()
      {
          if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
          {
              document.getElementById("district_id").innerHTML = xmlhttp.responseText;
          }
      }
      xmlhttp.open("GET", "<?php echo base_url(); ?>Teachers/get_all_district_by_divisionid?division=" + division, true);
      xmlhttp.send();
  }

  function get_all_thana_by_districtid(district){
        // alert(district);
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("thana_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>Teachers/get_all_thana_by_districtid?district=" + district, true);
        xmlhttp.send();
    }

    function get_all_union_by_thanaid(thana){
          // alert('thana');
          if (window.XMLHttpRequest)
          {
              xmlhttp = new XMLHttpRequest();
          }
          else
          {
              xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange = function()
          {
              if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
              {
                  document.getElementById("union_id").innerHTML = xmlhttp.responseText;
              }
          }
          xmlhttp.open("GET", "<?php echo base_url(); ?>Teachers/get_all_union_by_thanaid?thana=" + thana, true);
          xmlhttp.send();
      }

</script>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Teacher Add</title>
  </head>
  <br>
  <h3 align="center">Add Teacher</h3>
  <br>
  <body>
    <div class="contain-fluid">
      <form action="<?php echo base_url(); ?>Teachers/add" method="post">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Teacher Name</label>
      <input type="text" class="form-control" name="name" required>
    </div>
    <div class="form-group col-md-6">
      <label>Teacher ID</label>
      <input type="text" class="form-control" name="teacher_id" required>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Phone</label>
      <input type="number" class="form-control" name="phone" required>
    </div>
    <div class="form-group col-md-6">
      <label>Designation</label>
      <input type="text" class="form-control" name="designation" required>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Email</label>
      <input type="email" class="form-control" name="email" required>
    </div>
    <div class="form-group col-md-6">
      <label >Address</label>
      <input type="text" class="form-control" name="address" required>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-3">
      <label >Age</label>
      <input type="text" class="form-control" name="age" required>
    </div>
    <div class="form-group col-md-3">
      <label>School Name</label>
      <input type="text" class="form-control" name="school" required>
    </div>
    <div class="form-group col-md-3">
      <label >Gender</label><br>
      <input type="radio" id="male" name="gender" value="male" required>
      <label for="male">Male</label>
      <input type="radio" id="female" name="gender" value="female" required>
      <label for="female">Female</label><br>
    </div>
    <div class="form-group">
      <div class="form-check"><br>
        <input class="form-check-input" type="hidden" name="is_fulltime" value="0">
        <input class="form-check-input" type="checkbox" name="is_fulltime" value="1">
        <label class="form-check-label" for="is_fulltime" required>
          Is Fulltime?
        </label>
      </div>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-3">
      <label>Division</label>
      <select onchange="get_all_district_by_divisionid(this.value)" class="form-control"  name="division" required>
          <option value="">--Select--</option>
          <?php
          foreach ($division as $row):
          ?>
             <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
          <?php endforeach; ?>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label>District</label>
      <select onchange="get_all_thana_by_districtid(this.value)"  class="form-control"  name="district" id="district_id">
          <option value="">--Select--</option>
          <?php
          foreach ($district as $row):
          ?>
             <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
          <?php endforeach; ?>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label>Thana</label>
      <select onchange="get_all_union_by_thanaid(this.value)" class="form-control"  name="thana" id="thana_id">
          <option >--Select--</option>
          <?php
          foreach ($thana as $row):
          ?>
             <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
          <?php endforeach; ?>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label>Union</label>
      <select class="form-control"  name="union" id="union_id">
          <option value="">--Select--</option>
          <?php
          foreach ($union as $row):
          ?>
             <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
          <?php endforeach; ?>
      </select>
    </div>
  </div>
  <p align="center">
    <button class="btn btn-primary" type="submit">Add New</button>
  </p>

</form>
    </div>

  </body>
</html>
