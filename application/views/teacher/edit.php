<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Teacher Edit</title>
  </head>
  <h1>Edit Teacher Information</h1>
  <body>
    <form action="<?php echo base_url(); ?>Teachers/edit" method="post" align="center">
      <div class="contain-fluid">
        <input type="hidden" name="id" value="<?php echo $teachers_info->id?>"/><br><br>
        <span>Name</span>
        <input name="name" value="<?php echo $teachers_info->name?>"/><br><br>
        <span>Teacher ID</span>
        <input name="teacher_id" value="<?php echo $teachers_info->teacher_id?>"/><br><br>
        <span>Phone</span>
        <input name="phone" value="<?php echo $teachers_info->phone?>"/><br><br>
        <span>Designation</span>
        <input name="designation" value="<?php echo $teachers_info->designation?>"/><br><br>
        <span>Email</span>
        <input name="email" value="<?php echo $teachers_info->email?>"/><br><br>
        <span>Address</span>
        <input name="address" value="<?php echo $teachers_info->address?>"/><br><br>
        <span>School</span>
        <input name="school" value="<?php echo $teachers_info->school?>"/><br><br>
        <span>Age</span>
        <input name="age" value="<?php echo $teachers_info->age?>"/><br><br>
        <span>Gender</span>
        <input name="gender" value="<?php echo $teachers_info->gender?>"/><br><br>
        <span>Is Fulltime?</span>
        <input name="is_fulltime" value="<?php echo $teachers_info->is_fulltime?>"/><br><br>
        <span>Division</span>
        <input name="division" value="<?php echo $teachers_info->division?>"/><br><br>
        <span>District</span>
        <input name="district" value="<?php echo $teachers_info->district?>"/><br><br>
        <span>Thana</span>
        <input name="thana" value="<?php echo $teachers_info->thana?>"/><br><br>
        <span>Union</span>
        <input name="union" value="<?php echo $teachers_info->union?>"/><br>
      </div>

      <br>
      <div class="form-group">
          <button class="btn btn-primary" type="submit">Submit</button>
      </div>

    </form>
  </body>
</html>
