<?php

class Teacher extends CI_Model{

  function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function get_all_teachers()
    {
      $this->db->select('*');
      $this->db->from('tbl_teachers');
      $this->db->order_by('name','asc');
      $query = $this->db->get();
      return $query->result_array();
    }

    public function add_teachers($data)
        {
          return $this->db->insert('tbl_teachers', $data);
        }

    public function edit_teachers($data)
        {
          $this->db->where('id',$data['id']);
          $this->db->update('tbl_teachers', $data);
          return true;
        }

    public function get_teachers_by_id($id)
    {
      return $this->db->where('id',$id)->get('tbl_teachers')->row();
    }

    public function delete_teachers_id($id)
      {
        return $this->db->delete('tbl_teachers', array('id' => $id));
      }

      public function get_all_division()
      {
          return $this->db->get('tbl_division')->result_array();
      }

      public function get_all_district()
    {
        return $this->db->get('tbl_districts')->result_array();
    }

      public function get_all_district_by_divisionid($division)
    {
      return $this->db->where('division_id', $division)->get('tbl_districts')->result_array();
    }

    public function get_all_thana_by_districtid($district)
    {
      return $this->db->where('district_id', $district)->get('tbl_thana')->result_array();
    }

    public function get_all_union_by_thanaid($thana)
    {
      return $this->db->where('thana_id', $thana)->get('tbl_union')->result_array();
    }
  }

 ?>
